all: print_png print_test
print_png: print_png.o
	$(CC) `pkg-config --libs libpng` $? -o $@

print_png.o: print_png.c
	$(CC) $(CFLAGS) `pkg-config --cflags libpng` -c $? -o $@

print_test: print_test.o

clean:
	rm print_test print_png *.o

