	;; Forrás: Rádiótechnika 1984/11, 1984/12

DEFC	NMODEC=15
DEFC	GMODEC=8
DEFC	DMODEC=14
DEFC	CR=13
DEFC	ROM=2548	
DEFC	CHANEL=23749
DEFC	CA=95
DEFC	DA=31
DEFC	CB=127
DEFC	DB=63
DEFC	ERROR=5
DEFC	ACK=4
DEFC	BUSY=3

	ORG	50000
;A CHANNEL cimek atirasa
	JR	SKCONF
CONF:	DEFB	0
LCOUNT:	DEFB	175
SKCONF:	LD	HL,START
	LD	(CHANEL),HL
;PIO konfiguralasa
;PORT A 0-7 Data
;PORT B 0 - Strobe
;	1 - Test
;	2 - Init
;	3 - Busy
;	4 - Ack
;	5 - Error
	LD	A,255
	OUT	(CA),A
	INC	A
	OUT	(CA),A
	SET	ERROR,A
	SET	BUSY,A
	SET	ACK,A
	PUSH	AF
	LD	A,255
	OUT	(CB),A
	POP	AF
	OUT	(CB),A
	LD	A,7
	OUT	(DB),A
INIT:	LD	A,3
INIT1:	OUT	(DB),A
	LD	BC,5000
DELAY:	INC	A
	NOP
	JR	NZ,DELAY
	DEC	BC
	LD	A,B
	OR	C
	JR	NZ,DELAY
	LD	A,7
	OUT	(DB),A
	RET
TEST:	LD	A,5
	JR	INIT1
START:	CP	128		;Spectrum ROM keywords/raw codes
	JR	NC,ROMRAW
	CP	32		;ASCII chars
	JR	NC,PRINT
	CP	31
	JR	Z,COPY
	CP	30		;test
	JR	Z,TEST
	CP	29		;init
	JR	Z,INIT
	CP	27		;ESC
	JR	Z,PRINT
	CP	24		;cancel
	JR	Z,PRINT
	CP	17		;17-23, 25-26, 28, 31 invalid
	JR	NC,WHAT
	CP	12		;16 - POS, 15 - clear enlongation
	JR	NC,PRINT	;14 - enlongation, 13 - CR, 12 - FF
	CP	10		;LF
	JR	Z,PRINT
	CP	8		;BS
	JR	Z,PRINT
WHAT:	LD	A,'?'
	JR	PRINT
ROMRAW:	PUSH	AF
	LD	A,1
	LD	HL,CONF		;if conf=1, print raw
	AND	(HL)
	JR	Z,PRROM		;print keywords otherwise
	POP	AF
	JR	PRINT
PRROM:	POP	AF
	CP	160
	JP	NC,ROM		;skip codes 128-160, can't print graphics
	JR	WHAT		;characters and user defined graphics
NWLINE: LD	A,CR
PRINT:	PUSH	AF
	LD	A,7
	OUT	(DB),A
READ:	LD	A,$7F
	IN	A,($FE)
	RRA
	JR	C,ERRORS
	POP	AF
	RST	8
	DEFB	12
ERRORS:	IN	A,(DB)
	BIT	ERROR,A
	JR	NZ,BUSYS
	RST	8
	DEFB	18
BUSYS:	BIT	BUSY,A
	JR	NZ,READ
	POP	AF
	OUT	(DA),A
STROBE:	LD	A,6
	OUT	(DB),A
	LD	A,7
	OUT	(DB),A
	RET
COPY:	LD	A,175		;reset line counter (y)
	LD	(LCOUNT),A
	LD	A,27		;ESC
	CALL	PRINT
	LD	A,57		;9 - set line feed spacing
	CALL	PRINT
COPY1:	LD	A,(LCOUNT)
	LD	B,A		;B reg: y coord.
	LD	C,0		;C reg: x coord.
	LD	A,27
	CALL	PRINT
	LD	A,71
	CALL	PRINT
	LD	A,53
	CALL	PRINT
	LD	A,49
	CALL	PRINT
	LD	A,50
	CALL	PRINT		;ESC G 512 - 1 pass graphics mode, 256 cols
COPY1L:	PUSH	BC
	CALL	COPY1B
	PUSH	AF
	CALL	PRINT
	POP	AF		;print every column twice for more accurate
	CALL	PRINT		;aspect ratio
	POP	BC
	INC	C		;next column
	JR	NZ,COPY1L
	LD	A,13
	CALL	PRINT
	LD	HL,LCOUNT
	LD	A,(HL)
	CP	7		;last line reached when A = 7
	JR	Z,FINISH
	SUB	8
	LD	(HL),A		;save line count
	JR	COPY1
COPY1B:	LD	D,8
	LD	A,0
LOOP:	PUSH	AF
	CALL	POINT
	POP	AF
	SRA	E		;move pixel value into carry
	RR	A		;shift carry into MSB
	DEC	B
	DEC	D
	JR	NZ,LOOP
	RET
POINT:	PUSH	BC
	CALL	$22AA		;PIXEL ADDRESS subroutine (B max value 175)
	LD	B,A		;A = C mod 8
	INC	B
	LD	A,(HL)		;HL = address containing pixel specified by BC
LOOP2:	RLC	A		;shift the specified column to LSB
	DJNZ	LOOP2
	AND	1
	LD	E,A		;load pixel value into E
	POP	BC
	RET
FINISH:	LD	A,27
	CALL	PRINT
	LD	A,54		;ESC 6 - default line feed
	JP	PRINT		;print will ret
