#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <png.h>
#include <string.h>

#define ESC 27
#define CR 13
#define CLEAN_ERRNO() (errno == 0 ? "None" : strerror(errno))
#define CHECK_RET(x,y) do { \
    if(x<0){								\
      fprintf(stderr, "[ERROR] (%s:%d: " y ": %s) \n",			\
	      __FILE__, __LINE__, CLEAN_ERRNO());			\
      return 1;}							\
  } while(0)


png_bytep *row_pointers;
static int width, height, buf_width, buf_height;

void fill_ch(int row, int col, uint8_t **buf){
    uint8_t ch[8];
    /* Zero ch array*/
    for(int ii=0; ii<8; ii++)
        ch[ii]=0;
    /* Transform 8 lines to columns*/
    for(int jj=0; jj<8; jj++){
        for(int ii=0; ii<8; ii++){
            if((~row_pointers[row+jj][col/8])&(1<<(7-ii))){
                ch[ii]=(ch[ii]>>1) | (1<<7);
            } else {
                ch[ii]=ch[ii]>>1;
            }
        }
    }
    /*Copy ch into buffer*/
    for(int ii=0; ii<8; ii++){
        buf[row/8][col+ii]=ch[ii];
        /*printf("%d;",ch[ii]);*/
    }
    return;
}

uint8_t** read_png_file(char *filename, uint8_t **buf)
{
    FILE *fp = fopen(filename, "rb");
    if(fp == NULL)
        perror("fopen");

    png_structp png = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
    if(!png) abort();

    png_infop info = png_create_info_struct(png);
    if(!info) abort();

    if(setjmp(png_jmpbuf(png))) abort();

    png_init_io(png, fp);

    png_read_info(png, info);

    width=png_get_image_width(png, info);
    height=png_get_image_height(png, info);
    buf_height=height/8;
    buf_width=width-(width%8);

    /*Alloc print buffer*/
    buf=(uint8_t**)malloc(buf_height*sizeof(uint8_t*));
    for(int ii=0; ii<buf_height; ii++){
        buf[ii]=(uint8_t*)malloc(buf_width*sizeof(uint8_t));
    }

    /*Check image size*/
    if(width > 999){
        printf("Image width exceeds 999 px\n");
        exit(1);
    }

    /*Check file format*/
    if(png_get_bit_depth(png, info) != 1){
        printf("Incorrect image format");
        exit(1);
    }

    png_read_update_info(png, info);

    row_pointers = (png_bytep*)malloc(sizeof(png_bytep) * height);
    for(int y = 0; y < height; y++) {
        row_pointers[y] = (png_byte*)malloc(png_get_rowbytes(png, info));
    }

    png_read_image(png, row_pointers);

    for(int ii=0; ii<height/8; ii++){
        for(int jj=0; jj<width/8; jj++){
            fill_ch(ii*8, jj*8, buf);
        }
        /*printf("\n");*/
    }

    png_read_end(png, (png_infop)NULL);
    png_destroy_read_struct(&png, &info, (png_infopp)NULL);

    for(int ii=0; ii<height; ii++){
        free(row_pointers[ii]);
    }

    free(row_pointers);

    fclose(fp);
    return buf;
}

int set_line_feed(int fd)
{
    uint8_t esc=ESC;
    int ret;
    char line_feed[]="9";
    /*First, we send an ESC char*/
    ret=write(fd, &esc, 1);
    CHECK_RET(ret, "write");
    /*then we set the line feed (not including NUL at the end)*/
    write(fd, line_feed, 1);
    CHECK_RET(ret, "write");
    return 0;
}

int set_graph_mode(int fd, int line_length)
{
    uint8_t esc=ESC;
    char graph_mode[5];
    snprintf(graph_mode, 5, "G%03d", line_length);

    int ret;
    /*First, we send an ESC char*/
    ret=write(fd, &esc, 1);
    CHECK_RET(ret, "write");
    /*then we set the mode (not including NUL at the end)*/
    ret=write(fd, graph_mode, 4);
    CHECK_RET(ret, "write");
    return 0;
}

int main(int argc, char** argv)
{
    uint8_t **bufp=NULL;
    int cr=13, fd, ret;

    if(argc < 2) {
        fprintf(stderr, "Filename required\nUsage: %s filename\n", argv[0]);
	return 1;
    } else {
        bufp=read_png_file(argv[1], bufp);
    }

    fd=open("/dev/lp0", O_WRONLY);
    CHECK_RET(fd, "open");
    ret = set_line_feed(fd);
    if(ret != 0) {
        return 1;
    }

    /*Print line by line*/
    for(int ii=0; ii<buf_height; ii++){
        ret=set_graph_mode(fd,buf_width);
        if(ret != 0) {
            return 1;
        }
        write(fd, bufp[ii], buf_width);
        write(fd, &cr, 1);
    }

    for(int ii=0; ii<buf_height; ii++){
        free(bufp[ii]);
    }

    free(bufp);

    return 0;
}
/* vim: set tabstop=4 softtabstop=4 shiftwidth=4 expandtab : */
