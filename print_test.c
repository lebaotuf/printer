#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#define ESC 27
#define CR 13
#define CLEAN_ERRNO() (errno == 0 ? "None" : strerror(errno))
#define CHECK_RET(x,y) do { \
    if(x<0){								\
      fprintf(stderr, "[ERROR] (%s:%d: " y ": %s) \n",			\
	      __FILE__, __LINE__, CLEAN_ERRNO());			\
      return 1;}							\
  } while(0)

int set_line_feed(int fd)
{
    uint8_t esc=ESC;
    int ret;
    char line_feed[]="9";
    /*First, we send an ESC char*/
    ret=write(fd, &esc, 1);
    CHECK_RET(ret, "write");
    /*then we set the line feed (not including NUL at the end)*/
    write(fd, line_feed, 1);
    CHECK_RET(ret, "write");
    return 0;
}

int set_graph_mode(int fd, int line_length)
{
  uint8_t esc=ESC;
    char graph_mode[5];
    snprintf(graph_mode, 5, "G%03d", line_length);

    int ret;
    /*First, we send an ESC char*/
    ret=write(fd, &esc, 1);
    CHECK_RET(ret, "write");
    /*then we set the mode (not including NUL at the end)*/
    ret=write(fd, graph_mode, 4);
    CHECK_RET(ret, "write");
    return 0;
}

int main()
{
    int cr=13, fd, ret;
    int buf_long[999], buf_short[10];
    for(int ii=0; ii < 999; ii++)
        buf_long[ii]=85;
    for(int ii=0; ii < 10; ii++)
        buf_short[ii]=51;

    fd=open("/dev/lp0", O_WRONLY);
    CHECK_RET(fd, "open");
    ret=set_line_feed(fd);
    if(ret != 0) {
        return 1;
    }

    /*Print line by line*/
    ret=set_graph_mode(fd, 999);
    if(ret != 0) {
        return 1;
    }
    write(fd, buf_long, 999);

    ret=set_graph_mode(fd, 10);
    if(ret != 0) {
        return 1;
    }
    write(fd, buf_short, 10);

    write(fd, &cr, 1);

    return 0;
}
/* vim: set tabstop=4 softtabstop=4 shiftwidth=4 expandtab : */
